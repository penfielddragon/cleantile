chai = require 'chai'
should = chai.should()

requirejs = require "requirejs"

testGeneral = (name, CleanTile) ->
  describe name, ->
    it "should load the class", ->
      should.exist CleanTile
      CleanTile.should.be.a "function"
    it "should have options", ->
      tiler = new CleanTile()
      should.exist tiler
      should.exist tiler.config
      should.exist tiler.config()
      should.exist tiler.config().config.layouts
      should.exist tiler.config().config.layouts.useDefaults

testRequireJS = (file) ->
  requirejs ["#{__dirname}/../build/#{file}.js"], (clean) ->
    testGeneral "Loading #{file} via RequireJS", clean

testNode = (file) ->
  testGeneral "Loading #{file} via Node.js", require("#{__dirname}/../build/#{file}.js")

exts = ["", ".nolib", ".inline", ".min", ".inline.nolib.min"]

testRequireJS "cleantile-#{env}#{ext}" for ext in exts for env in ["browser"] #"universal"
testNode "cleantile-#{env}#{ext}" for ext in exts for env in ["universal", "node"]
