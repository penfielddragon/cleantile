Multiple formats of built files are created, depending on the options passed during compile.  By default, all options
listed below are included in this repository.

TLDR: Try `cleantile-universal.min.js`.  It might not work with [RequireJS](http://requirejs.org/), in which case, try
`cleantile-browser.min.js`.

If you are already using libraries such as jQuery or lodash, keep reading to see how to avoid a larger-than-needed file.
Also keep reading if you need to debug something and want sourcemaps.

## Browser vs. Node.js (vs. Universal vs. Bare)

A major distinction in files is the target environment.  Versions are written for [RequireJS](http://requirejs.org/),
[CommonJS](http://www.commonjs.org/) (used by [Node.js](https://nodejs.org/en/)), as well as a bare version, made to be
run without any loading library, such as directly in a `<script>` tag.  If that wasn't enough, there's also a
[UMD](https://github.com/umdjs/umd) version that includes a wrapper for each of these methods, and tries to detect which
to use.  However, some bugs have been found when using the universal version, so I recommend using one more finely-tuned
for your environment.

Here's all of the formats.  See below for descriptions of `inline`, `nolib`, and `min`.

AMD/RequireJS: `cleantile-browser[.inline][.nolib][.min].js`

CommonJS/Node: `cleantile-node[.inline][.nolib][.min].js`

Bare/Global Scope: `cleantile[.inline][.nolib][.min].js`

Universal/UMD: `cleantile-universal[.inline][.nolib][.min].js`

## Inline Source Maps

Files with `.inline` in the extension have an internal sourcemap, which is useful for development and debugging.
Otherwise, most files have a `.js.map` version.

## No-Library Versions

Files with `.nolib` in the extension do not include several libraries directly in the source, instead using standard
loaders.  In a Node.js environment, the libraries will be found in `node_modules`.  With RequireJS, the path will have
to be defined.

Non-Included Libraries:
- jQuery
- lodash

## Minified Versions

Files with `.min` in the extension have been minfied via [Uglify](https://github.com/mishoo/UglifyJS2) to reduce
filesize.
