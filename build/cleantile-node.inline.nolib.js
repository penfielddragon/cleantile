/*
CleanTile v0.0.2
Copyright Ryan Leonard.  See https://bitbucket.org/penfielddragon/cleantile#readme for LICENSE.
*/
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var require$$1 = _interopDefault(require('lodash'));

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var AppSource = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0

/*
Defines a place to look for applications.  Designed to be extended, either through CoffeeScript extension or by setting
properties.

@example CoffeeScript Extension
  class RunningApps extends AppSource
    constructor: ->
       * Open a subset of apps, etc.
      @_apps = [new SettingsPane(), new TextEditorPane(), new CommentsPane(), new TextControlsPane()]
    snapshot: -> @_apps

@example JavaScript Extension
  `
  var settings = {name: "Settings", icon: "<i class='fa fa-cog'></i>"};
  var textEditor = {name: "Markdown Editor", icon: "<i class='fa fa-file-text-o'></i>"};
  var commentsPane = {name: "Comments Settings", icon: "<i class='fa fa-comment-o'></i>"};
  var textControls = {name: "Text Formatting", icon: "<i class='fa fa-sliders'></i>"};
  var runningApps = new AppSource();
  runningApps.onSnapshot(function() {
    return [settings, textEditor, commentsPane, textControls];
  });
  `
 */
var AppSource;

AppSource = (function() {
  function AppSource() {}


  /*
  Called by CleanTile to determine which applications can be inserted into panes.
  @abstract Override with a function that returns the current apps
  @return {Array<Application>, Promise<Array<Application>>} return either a list of applications, or a promise to return
  applications in the future.
   */

  AppSource.prototype.snapshot = function() {
    return [];
  };


  /*
  Set AppSource.snapshot on the fly.
  @see AppSource.snapshot AppSource.snapshot for description of functionality and types involved.
  @param {Function} fn a new snapshot function
   */

  AppSource.prototype.onSnapshot = function(fn) {
    return this.snapshot = fn;
  };

  return AppSource;

})();

module.exports = AppSource;

});

var require$$0$1 = (AppSource && typeof AppSource === 'object' && 'default' in AppSource ? AppSource['default'] : AppSource);

var StaticAppSource = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0
var AppSource, StaticAppSource,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty,
  slice = [].slice;

AppSource = require$$0$1;


/*
Wraps a constant list of applications as an app source.
 */

StaticAppSource = (function(superClass) {
  extend(StaticAppSource, superClass);


  /*
  @param {Array<Application>} apps the applications to display to the user.
   */

  function StaticAppSource() {
    var apps;
    apps = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    this.apps = apps;
  }


  /*
  @see AppSource.snapshot
   */

  StaticAppSource.prototype.snapshot = function() {
    return this.apps;
  };

  return StaticAppSource;

})(AppSource);

module.exports = StaticAppSource;

});

var require$$0 = (StaticAppSource && typeof StaticAppSource === 'object' && 'default' in StaticAppSource ? StaticAppSource['default'] : StaticAppSource);

var StaticLayoutSource = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0

/*
Defines a layout source that handles the static strings in the configuration object.
 */
var StaticLayoutSource;

StaticLayoutSource = (function() {
  function StaticLayoutSource() {}

  return StaticLayoutSource;

})();

module.exports = StaticLayoutSource;

});

var require$$0$2 = (StaticLayoutSource && typeof StaticLayoutSource === 'object' && 'default' in StaticLayoutSource ? StaticLayoutSource['default'] : StaticLayoutSource);

var Configuration = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0
var Configuration, StaticLayoutSource, _, defaults;

_ = require$$1;

StaticLayoutSource = require$$0$2;

defaults = {
  layouts: {
    useDefaults: true,
    "static": {
      "Single": "{}",
      "2 Columns": "{'horiz': true, split: '50%'}",
      "3 Columns": "{'horiz': true, split: '33%', right: {'horiz': true, split: '33%'}}",
      "2 Rows": "{'horiz': false, split: '50%'}",
      "3 Rows": "{'horiz': false, split: '33%', right: {'horiz': false, split: '33%'}}"
    },
    sources: {
      "Default Layouts": StaticLayoutSource
    }
  },
  apps: {
    sources: {}
  }
};


/*
Stores and sets configuration values.
 */

Configuration = (function() {

  /*
  @param {Object} options properties to set in the configuration.
   */
  function Configuration(options) {
    if (options == null) {
      options = {};
    }
    this.config = _.defaultsDeep({}, options, defaults);
  }


  /*
  Get the value of a property by name.
  @param {String} key the name of a property
  @return {Anything} the stored value
   */

  Configuration.prototype.get = function(key) {
    return this.config[key];
  };


  /*
  Set the value of a single property.
  @param {String} key the name of a property
  @param {Anything} the value to store
   */

  Configuration.prototype.set = function(key, val) {
    return this.config[key] = val;
  };


  /*
  Update multiple properties.
  @param {Object} obj property names and associated values to set.
   */

  Configuration.prototype.extend = function(obj) {
    return this.config = _.defaultsDeep({}, obj, this.config);
  };

  return Configuration;

})();

module.exports = Configuration;

});

var require$$2 = (Configuration && typeof Configuration === 'object' && 'default' in Configuration ? Configuration['default'] : Configuration);

var Organizer = createCommonjsModule(function (module) {
// Generated by CoffeeScript 1.10.0
var AppSource, Configuration, Organizer, StaticAppSource,
  slice = [].slice;

Configuration = require$$2;

AppSource = require$$0$1;

StaticAppSource = require$$0;


/*
Main interface to support user's editing instances of CleanTile.
 */

Organizer = (function() {
  Organizer.AppSource = AppSource;


  /*
  @param {Object} options properties to set in the configuration.
   */

  function Organizer(options) {
    this._config = new Configuration(options);
  }


  /*
  Changes the source evaluated to find applications.
  @param {AppSource} source the new source for applications
  @return {Organizer}
   */

  Organizer.prototype.source = function(source) {
    this._appsource = source;
    return this;
  };


  /*
  Sets the app source to a static list of applications.
  
  @overload apps(apps)
    Use a list of applications
    @param {Array<Application>} apps all the applications to load
    @return {Organizer}
  
  @overload apps(app1, app2)
    Pass each application as a parameter
    @param {Application} app1 a single application to load
    @param {Application} app2 a single application to load
    @return {Organizer}
   */

  Organizer.prototype.apps = function() {
    var apps;
    apps = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    if (apps && apps.length && apps.length === 1 && apps[0].length) {
      apps = apps[0];
    }
    this.source(new StaticAppSource(apps));
    return this;
  };


  /*
  Deals with configuration.
  
  @overload config()
    @return {Configuration} The entire configuration object
  
  @overload config(key)
    Retrieve a single property's value
    @param {String} key the name of a property to read
    @return {Anything} the value stored in the configuration
  
  @overload config(key, val)
    Set a single property's value
    @param {String} key the name of a property to set
    @param {Anything} val the value to store
    @return {Organizer}
  
  @overload config(obj)
    Update multiple properties.
    @param {Object} obj names and values of configuration properties to set
    @return {Organizer}
   */

  Organizer.prototype.config = function(key, val) {
    if (!key && !val) {
      return this._config;
    }
    if (!val && typeof key !== "object") {
      this._config.get(key);
    } else if (!val) {
      this._config.extend(key);
    } else {
      this._config.set(key, val);
    }
    return this;
  };

  return Organizer;

})();

module.exports = Organizer;

});

var Organizer$1 = (Organizer && typeof Organizer === 'object' && 'default' in Organizer ? Organizer['default'] : Organizer);

module.exports = Organizer$1;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjpudWxsLCJzb3VyY2VzIjpbIi4uL3NyYy9BcHBTb3VyY2UuY29mZmVlIiwiLi4vc3JjL1N0YXRpY0FwcFNvdXJjZS5jb2ZmZWUiLCIuLi9zcmMvU3RhdGljTGF5b3V0U291cmNlLmNvZmZlZSIsIi4uL3NyYy9Db25maWd1cmF0aW9uLmNvZmZlZSIsIi4uL3NyYy9Pcmdhbml6ZXIuY29mZmVlIl0sInNvdXJjZXNDb250ZW50IjpbIiMjI1xuRGVmaW5lcyBhIHBsYWNlIHRvIGxvb2sgZm9yIGFwcGxpY2F0aW9ucy4gIERlc2lnbmVkIHRvIGJlIGV4dGVuZGVkLCBlaXRoZXIgdGhyb3VnaCBDb2ZmZWVTY3JpcHQgZXh0ZW5zaW9uIG9yIGJ5IHNldHRpbmdcbnByb3BlcnRpZXMuXG5cbkBleGFtcGxlIENvZmZlZVNjcmlwdCBFeHRlbnNpb25cbiAgY2xhc3MgUnVubmluZ0FwcHMgZXh0ZW5kcyBBcHBTb3VyY2VcbiAgICBjb25zdHJ1Y3RvcjogLT5cbiAgICAgICMgT3BlbiBhIHN1YnNldCBvZiBhcHBzLCBldGMuXG4gICAgICBAX2FwcHMgPSBbbmV3IFNldHRpbmdzUGFuZSgpLCBuZXcgVGV4dEVkaXRvclBhbmUoKSwgbmV3IENvbW1lbnRzUGFuZSgpLCBuZXcgVGV4dENvbnRyb2xzUGFuZSgpXVxuICAgIHNuYXBzaG90OiAtPiBAX2FwcHNcblxuQGV4YW1wbGUgSmF2YVNjcmlwdCBFeHRlbnNpb25cbiAgYFxuICB2YXIgc2V0dGluZ3MgPSB7bmFtZTogXCJTZXR0aW5nc1wiLCBpY29uOiBcIjxpIGNsYXNzPSdmYSBmYS1jb2cnPjwvaT5cIn07XG4gIHZhciB0ZXh0RWRpdG9yID0ge25hbWU6IFwiTWFya2Rvd24gRWRpdG9yXCIsIGljb246IFwiPGkgY2xhc3M9J2ZhIGZhLWZpbGUtdGV4dC1vJz48L2k+XCJ9O1xuICB2YXIgY29tbWVudHNQYW5lID0ge25hbWU6IFwiQ29tbWVudHMgU2V0dGluZ3NcIiwgaWNvbjogXCI8aSBjbGFzcz0nZmEgZmEtY29tbWVudC1vJz48L2k+XCJ9O1xuICB2YXIgdGV4dENvbnRyb2xzID0ge25hbWU6IFwiVGV4dCBGb3JtYXR0aW5nXCIsIGljb246IFwiPGkgY2xhc3M9J2ZhIGZhLXNsaWRlcnMnPjwvaT5cIn07XG4gIHZhciBydW5uaW5nQXBwcyA9IG5ldyBBcHBTb3VyY2UoKTtcbiAgcnVubmluZ0FwcHMub25TbmFwc2hvdChmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gW3NldHRpbmdzLCB0ZXh0RWRpdG9yLCBjb21tZW50c1BhbmUsIHRleHRDb250cm9sc107XG4gIH0pO1xuICBgXG4jIyNcbmNsYXNzIEFwcFNvdXJjZVxuICAjIyNcbiAgQ2FsbGVkIGJ5IENsZWFuVGlsZSB0byBkZXRlcm1pbmUgd2hpY2ggYXBwbGljYXRpb25zIGNhbiBiZSBpbnNlcnRlZCBpbnRvIHBhbmVzLlxuICBAYWJzdHJhY3QgT3ZlcnJpZGUgd2l0aCBhIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyB0aGUgY3VycmVudCBhcHBzXG4gIEByZXR1cm4ge0FycmF5PEFwcGxpY2F0aW9uPiwgUHJvbWlzZTxBcnJheTxBcHBsaWNhdGlvbj4+fSByZXR1cm4gZWl0aGVyIGEgbGlzdCBvZiBhcHBsaWNhdGlvbnMsIG9yIGEgcHJvbWlzZSB0byByZXR1cm5cbiAgYXBwbGljYXRpb25zIGluIHRoZSBmdXR1cmUuXG4gICMjI1xuICBzbmFwc2hvdDogLT4gW11cblxuICAjIyNcbiAgU2V0IEFwcFNvdXJjZS5zbmFwc2hvdCBvbiB0aGUgZmx5LlxuICBAc2VlIEFwcFNvdXJjZS5zbmFwc2hvdCBBcHBTb3VyY2Uuc25hcHNob3QgZm9yIGRlc2NyaXB0aW9uIG9mIGZ1bmN0aW9uYWxpdHkgYW5kIHR5cGVzIGludm9sdmVkLlxuICBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBhIG5ldyBzbmFwc2hvdCBmdW5jdGlvblxuICAjIyNcbiAgb25TbmFwc2hvdDogKGZuKSAtPiBAc25hcHNob3QgPSBmblxuXG5tb2R1bGUuZXhwb3J0cyA9IEFwcFNvdXJjZVxuIiwiQXBwU291cmNlID0gcmVxdWlyZSBcIi4vQXBwU291cmNlXCJcblxuIyMjXG5XcmFwcyBhIGNvbnN0YW50IGxpc3Qgb2YgYXBwbGljYXRpb25zIGFzIGFuIGFwcCBzb3VyY2UuXG4jIyNcbmNsYXNzIFN0YXRpY0FwcFNvdXJjZSBleHRlbmRzIEFwcFNvdXJjZVxuICAjIyNcbiAgQHBhcmFtIHtBcnJheTxBcHBsaWNhdGlvbj59IGFwcHMgdGhlIGFwcGxpY2F0aW9ucyB0byBkaXNwbGF5IHRvIHRoZSB1c2VyLlxuICAjIyNcbiAgY29uc3RydWN0b3I6IChAYXBwcy4uLikgLT5cblxuICAjIyNcbiAgQHNlZSBBcHBTb3VyY2Uuc25hcHNob3RcbiAgIyMjXG4gIHNuYXBzaG90OiAtPiBAYXBwc1xuXG5tb2R1bGUuZXhwb3J0cyA9IFN0YXRpY0FwcFNvdXJjZVxuIiwiIyMjXG5EZWZpbmVzIGEgbGF5b3V0IHNvdXJjZSB0aGF0IGhhbmRsZXMgdGhlIHN0YXRpYyBzdHJpbmdzIGluIHRoZSBjb25maWd1cmF0aW9uIG9iamVjdC5cbiMjI1xuY2xhc3MgU3RhdGljTGF5b3V0U291cmNlXG5cbm1vZHVsZS5leHBvcnRzID0gU3RhdGljTGF5b3V0U291cmNlXG4iLCJfID0gcmVxdWlyZSBcImxvZGFzaFwiXG5cblN0YXRpY0xheW91dFNvdXJjZSA9IHJlcXVpcmUgXCIuL1N0YXRpY0xheW91dFNvdXJjZVwiXG5cbmRlZmF1bHRzID1cbiAgbGF5b3V0czpcbiAgICB1c2VEZWZhdWx0czogeWVzXG4gICAgc3RhdGljOlxuICAgICAgXCJTaW5nbGVcIjogXCJ7fVwiXG4gICAgICBcIjIgQ29sdW1uc1wiOiBcInsnaG9yaXonOiB0cnVlLCBzcGxpdDogJzUwJSd9XCJcbiAgICAgIFwiMyBDb2x1bW5zXCI6IFwieydob3Jpeic6IHRydWUsIHNwbGl0OiAnMzMlJywgcmlnaHQ6IHsnaG9yaXonOiB0cnVlLCBzcGxpdDogJzMzJSd9fVwiXG4gICAgICBcIjIgUm93c1wiOiBcInsnaG9yaXonOiBmYWxzZSwgc3BsaXQ6ICc1MCUnfVwiXG4gICAgICBcIjMgUm93c1wiOiBcInsnaG9yaXonOiBmYWxzZSwgc3BsaXQ6ICczMyUnLCByaWdodDogeydob3Jpeic6IGZhbHNlLCBzcGxpdDogJzMzJSd9fVwiXG4gICAgc291cmNlczpcbiAgICAgIFwiRGVmYXVsdCBMYXlvdXRzXCI6IFN0YXRpY0xheW91dFNvdXJjZVxuICBhcHBzOlxuICAgIHNvdXJjZXM6IHt9XG5cbiMjI1xuU3RvcmVzIGFuZCBzZXRzIGNvbmZpZ3VyYXRpb24gdmFsdWVzLlxuIyMjXG5jbGFzcyBDb25maWd1cmF0aW9uXG4gICMjI1xuICBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyBwcm9wZXJ0aWVzIHRvIHNldCBpbiB0aGUgY29uZmlndXJhdGlvbi5cbiAgIyMjXG4gIGNvbnN0cnVjdG9yOiAob3B0aW9ucyA9IHt9KSAtPlxuICAgIEBjb25maWcgPSBfLmRlZmF1bHRzRGVlcCB7fSwgb3B0aW9ucywgZGVmYXVsdHNcblxuICAjIyNcbiAgR2V0IHRoZSB2YWx1ZSBvZiBhIHByb3BlcnR5IGJ5IG5hbWUuXG4gIEBwYXJhbSB7U3RyaW5nfSBrZXkgdGhlIG5hbWUgb2YgYSBwcm9wZXJ0eVxuICBAcmV0dXJuIHtBbnl0aGluZ30gdGhlIHN0b3JlZCB2YWx1ZVxuICAjIyNcbiAgZ2V0OiAoa2V5KSAtPiBAY29uZmlnW2tleV1cblxuICAjIyNcbiAgU2V0IHRoZSB2YWx1ZSBvZiBhIHNpbmdsZSBwcm9wZXJ0eS5cbiAgQHBhcmFtIHtTdHJpbmd9IGtleSB0aGUgbmFtZSBvZiBhIHByb3BlcnR5XG4gIEBwYXJhbSB7QW55dGhpbmd9IHRoZSB2YWx1ZSB0byBzdG9yZVxuICAjIyNcbiAgc2V0OiAoa2V5LCB2YWwpIC0+IEBjb25maWdba2V5XSA9IHZhbFxuXG4gICMjI1xuICBVcGRhdGUgbXVsdGlwbGUgcHJvcGVydGllcy5cbiAgQHBhcmFtIHtPYmplY3R9IG9iaiBwcm9wZXJ0eSBuYW1lcyBhbmQgYXNzb2NpYXRlZCB2YWx1ZXMgdG8gc2V0LlxuICAjIyNcbiAgZXh0ZW5kOiAob2JqKSAtPiBAY29uZmlnID0gXy5kZWZhdWx0c0RlZXAge30sIG9iaiwgQGNvbmZpZ1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvbmZpZ3VyYXRpb25cbiIsIkNvbmZpZ3VyYXRpb24gPSByZXF1aXJlIFwiLi9Db25maWd1cmF0aW9uXCJcbkFwcFNvdXJjZSA9IHJlcXVpcmUgXCIuL0FwcFNvdXJjZVwiXG5TdGF0aWNBcHBTb3VyY2UgPSByZXF1aXJlIFwiLi9TdGF0aWNBcHBTb3VyY2VcIlxuXG4jIyNcbk1haW4gaW50ZXJmYWNlIHRvIHN1cHBvcnQgdXNlcidzIGVkaXRpbmcgaW5zdGFuY2VzIG9mIENsZWFuVGlsZS5cbiMjI1xuY2xhc3MgT3JnYW5pemVyXG4gICMgQHNlZSBBcHBTb3VyY2VcbiAgQEFwcFNvdXJjZTogQXBwU291cmNlXG4gICMjI1xuICBAcGFyYW0ge09iamVjdH0gb3B0aW9ucyBwcm9wZXJ0aWVzIHRvIHNldCBpbiB0aGUgY29uZmlndXJhdGlvbi5cbiAgIyMjXG4gIGNvbnN0cnVjdG9yOiAob3B0aW9ucykgLT5cbiAgICBAX2NvbmZpZyA9IG5ldyBDb25maWd1cmF0aW9uIG9wdGlvbnNcblxuICAjIyNcbiAgQ2hhbmdlcyB0aGUgc291cmNlIGV2YWx1YXRlZCB0byBmaW5kIGFwcGxpY2F0aW9ucy5cbiAgQHBhcmFtIHtBcHBTb3VyY2V9IHNvdXJjZSB0aGUgbmV3IHNvdXJjZSBmb3IgYXBwbGljYXRpb25zXG4gIEByZXR1cm4ge09yZ2FuaXplcn1cbiAgIyMjXG4gIHNvdXJjZTogKHNvdXJjZSkgLT5cbiAgICBAX2FwcHNvdXJjZSA9IHNvdXJjZVxuICAgIEBcblxuICAjIyNcbiAgU2V0cyB0aGUgYXBwIHNvdXJjZSB0byBhIHN0YXRpYyBsaXN0IG9mIGFwcGxpY2F0aW9ucy5cblxuICBAb3ZlcmxvYWQgYXBwcyhhcHBzKVxuICAgIFVzZSBhIGxpc3Qgb2YgYXBwbGljYXRpb25zXG4gICAgQHBhcmFtIHtBcnJheTxBcHBsaWNhdGlvbj59IGFwcHMgYWxsIHRoZSBhcHBsaWNhdGlvbnMgdG8gbG9hZFxuICAgIEByZXR1cm4ge09yZ2FuaXplcn1cblxuICBAb3ZlcmxvYWQgYXBwcyhhcHAxLCBhcHAyKVxuICAgIFBhc3MgZWFjaCBhcHBsaWNhdGlvbiBhcyBhIHBhcmFtZXRlclxuICAgIEBwYXJhbSB7QXBwbGljYXRpb259IGFwcDEgYSBzaW5nbGUgYXBwbGljYXRpb24gdG8gbG9hZFxuICAgIEBwYXJhbSB7QXBwbGljYXRpb259IGFwcDIgYSBzaW5nbGUgYXBwbGljYXRpb24gdG8gbG9hZFxuICAgIEByZXR1cm4ge09yZ2FuaXplcn1cbiAgIyMjXG4gIGFwcHM6IChhcHBzLi4uKSAtPlxuICAgIGFwcHMgPSBhcHBzWzBdIGlmIGFwcHMgYW5kIGFwcHMubGVuZ3RoIGFuZCBhcHBzLmxlbmd0aCBpcyAxIGFuZCBhcHBzWzBdLmxlbmd0aFxuICAgIEBzb3VyY2UgbmV3IFN0YXRpY0FwcFNvdXJjZSBhcHBzXG4gICAgQFxuXG4gICMjI1xuICBEZWFscyB3aXRoIGNvbmZpZ3VyYXRpb24uXG5cbiAgQG92ZXJsb2FkIGNvbmZpZygpXG4gICAgQHJldHVybiB7Q29uZmlndXJhdGlvbn0gVGhlIGVudGlyZSBjb25maWd1cmF0aW9uIG9iamVjdFxuXG4gIEBvdmVybG9hZCBjb25maWcoa2V5KVxuICAgIFJldHJpZXZlIGEgc2luZ2xlIHByb3BlcnR5J3MgdmFsdWVcbiAgICBAcGFyYW0ge1N0cmluZ30ga2V5IHRoZSBuYW1lIG9mIGEgcHJvcGVydHkgdG8gcmVhZFxuICAgIEByZXR1cm4ge0FueXRoaW5nfSB0aGUgdmFsdWUgc3RvcmVkIGluIHRoZSBjb25maWd1cmF0aW9uXG5cbiAgQG92ZXJsb2FkIGNvbmZpZyhrZXksIHZhbClcbiAgICBTZXQgYSBzaW5nbGUgcHJvcGVydHkncyB2YWx1ZVxuICAgIEBwYXJhbSB7U3RyaW5nfSBrZXkgdGhlIG5hbWUgb2YgYSBwcm9wZXJ0eSB0byBzZXRcbiAgICBAcGFyYW0ge0FueXRoaW5nfSB2YWwgdGhlIHZhbHVlIHRvIHN0b3JlXG4gICAgQHJldHVybiB7T3JnYW5pemVyfVxuXG4gIEBvdmVybG9hZCBjb25maWcob2JqKVxuICAgIFVwZGF0ZSBtdWx0aXBsZSBwcm9wZXJ0aWVzLlxuICAgIEBwYXJhbSB7T2JqZWN0fSBvYmogbmFtZXMgYW5kIHZhbHVlcyBvZiBjb25maWd1cmF0aW9uIHByb3BlcnRpZXMgdG8gc2V0XG4gICAgQHJldHVybiB7T3JnYW5pemVyfVxuICAjIyNcbiAgY29uZmlnOiAoa2V5LCB2YWwpIC0+XG4gICAgaWYgbm90IGtleSBhbmQgbm90IHZhbFxuICAgICAgcmV0dXJuIEBfY29uZmlnXG4gICAgaWYgbm90IHZhbCBhbmQgdHlwZW9mIGtleSBpc250IFwib2JqZWN0XCJcbiAgICAgIEBfY29uZmlnLmdldChrZXkpXG4gICAgZWxzZSBpZiBub3QgdmFsXG4gICAgICBAX2NvbmZpZy5leHRlbmQoa2V5KVxuICAgIGVsc2VcbiAgICAgIEBfY29uZmlnLnNldCBrZXksIHZhbFxuICAgIEBcblxubW9kdWxlLmV4cG9ydHMgPSBPcmdhbml6ZXJcbiJdLCJuYW1lcyI6WyJyZXF1aXJlJCQwIiwicmVxdWlyZSQkMSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQUE7O0FBdUJNOzs7Ozs7Ozs7OztzQkFPSixRQUFBLEdBQVU7V0FBRzs7Ozs7Ozs7OztzQkFPYixVQUFBLEdBQVksU0FBQyxFQUFEO1dBQVEsSUFBQyxDQUFBLFFBQUQsR0FBWTs7Ozs7OztBQUVsQyxNQUFNLENBQUMsT0FBUCxHQUFpQjs7Ozs7Ozs7QUN2Q2pCLElBQUEsMEJBQUE7RUFBQTs7OztBQUFBLFNBQUEsR0FBWUE7Ozs7Ozs7QUFLTjs7Ozs7Ozs7RUFJUztRQUFZO0lBQVg7SUFBQSxJQUFDLENBQUEsT0FBRDs7Ozs7Ozs7NEJBS2QsUUFBQSxHQUFVO1dBQUcsSUFBQyxDQUFBOzs7OztHQVRjOztBQVc5QixNQUFNLENBQUMsT0FBUCxHQUFpQjs7Ozs7Ozs7Ozs7O0FDaEJqQixJQUFBOztBQUdNOzs7Ozs7O0FBRU4sTUFBTSxDQUFDLE9BQVAsR0FBaUI7Ozs7Ozs7O0FDTGpCLElBQUE7O0FBQUEsQ0FBQSxHQUFJOztBQUVKLGtCQUFBLEdBQXFCQTs7QUFFckIsUUFBQSxHQUNFO0VBQUEsT0FBQSxFQUNFO0lBQUEsV0FBQSxFQUFhLElBQWI7SUFDQSxRQUFBLEVBQ0U7TUFBQSxRQUFBLEVBQVUsSUFBVjtNQUNBLFdBQUEsRUFBYSwrQkFEYjtNQUVBLFdBQUEsRUFBYSxxRUFGYjtNQUdBLFFBQUEsRUFBVSxnQ0FIVjtNQUlBLFFBQUEsRUFBVSx1RUFKVjtLQUZGO0lBT0EsT0FBQSxFQUNFO01BQUEsaUJBQUEsRUFBbUIsa0JBQW5CO0tBUkY7R0FERjtFQVVBLElBQUEsRUFDRTtJQUFBLE9BQUEsRUFBUyxFQUFUO0dBWEY7Ozs7Ozs7O0FBZ0JJOzs7OztFQUlTLHVCQUFDLE9BQUQ7O01BQUMsVUFBVTs7SUFDdEIsSUFBQyxDQUFBLE1BQUQsR0FBVSxDQUFDLENBQUMsWUFBRixDQUFlLEVBQWYsRUFBbUIsT0FBbkIsRUFBNEIsUUFBNUI7Ozs7Ozs7Ozs7MEJBT1osR0FBQSxHQUFLLFNBQUMsR0FBRDtXQUFTLElBQUMsQ0FBQSxNQUFPLENBQUEsR0FBQTs7Ozs7Ozs7OzswQkFPdEIsR0FBQSxHQUFLLFNBQUMsR0FBRCxFQUFNLEdBQU47V0FBYyxJQUFDLENBQUEsTUFBTyxDQUFBLEdBQUEsQ0FBUixHQUFlOzs7Ozs7Ozs7MEJBTWxDLE1BQUEsR0FBUSxTQUFDLEdBQUQ7V0FBUyxJQUFDLENBQUEsTUFBRCxHQUFVLENBQUMsQ0FBQyxZQUFGLENBQWUsRUFBZixFQUFtQixHQUFuQixFQUF3QixJQUFDLENBQUEsTUFBekI7Ozs7Ozs7QUFFN0IsTUFBTSxDQUFDLE9BQVAsR0FBaUI7Ozs7Ozs7O0FDaERqQixJQUFBLG9EQUFBO0VBQUE7O0FBQUEsYUFBQSxHQUFnQjs7QUFDaEIsU0FBQSxHQUFZQzs7QUFDWixlQUFBLEdBQWtCOzs7Ozs7O0FBS1o7RUFFSixTQUFDLENBQUEsU0FBRCxHQUFZOzs7Ozs7O0VBSUMsbUJBQUMsT0FBRDtJQUNYLElBQUMsQ0FBQSxPQUFELEdBQWUsSUFBQSxhQUFBLENBQWMsT0FBZDs7Ozs7Ozs7OztzQkFPakIsTUFBQSxHQUFRLFNBQUMsTUFBRDtJQUNOLElBQUMsQ0FBQSxVQUFELEdBQWM7V0FDZDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztzQkFnQkYsSUFBQSxHQUFNO1FBQ0o7SUFESztJQUNMLElBQWtCLElBQUEsSUFBUyxJQUFJLENBQUMsTUFBZCxJQUF5QixJQUFJLENBQUMsTUFBTCxLQUFlLENBQXhDLElBQThDLElBQUssQ0FBQSxDQUFBLENBQUUsQ0FBQyxNQUF4RTtNQUFBLElBQUEsR0FBTyxJQUFLLENBQUEsQ0FBQSxFQUFaOztJQUNBLElBQUMsQ0FBQSxNQUFELENBQVksSUFBQSxlQUFBLENBQWdCLElBQWhCLENBQVo7V0FDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NCQXdCRixNQUFBLEdBQVEsU0FBQyxHQUFELEVBQU0sR0FBTjtJQUNOLElBQUcsQ0FBSSxHQUFKLElBQVksQ0FBSSxHQUFuQjthQUNTLElBQUMsQ0FBQSxRQURWOztJQUVBLElBQUcsQ0FBSSxHQUFKLElBQVksT0FBTyxHQUFQLEtBQWdCLFFBQS9CO01BQ0UsSUFBQyxDQUFBLE9BQU8sQ0FBQyxHQUFULENBQWEsR0FBYixFQURGO0tBQUEsTUFFSyxJQUFHLENBQUksR0FBUDtNQUNILElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxDQUFnQixHQUFoQixFQURHO0tBQUEsTUFBQTtNQUdILElBQUMsQ0FBQSxPQUFPLENBQUMsR0FBVCxDQUFhLEdBQWIsRUFBa0IsR0FBbEIsRUFIRzs7V0FJTDs7Ozs7OztBQUVKLE1BQU0sQ0FBQyxPQUFQLEdBQWlCOzs7Ozs7In0=