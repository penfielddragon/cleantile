_ = require "underscore"
Promise = require "bluebird"

{exec} = require "child-process-promise"
rimraf = require "rimraf-promise"
replace = require "replace"

rollup = require "rollup"
commonjs = require "rollup-plugin-commonjs"
coffee = require "rollup-plugin-coffee-script"
nodeResolve = require "rollup-plugin-node-resolve"
uglify = require "rollup-plugin-uglify"
sourcemaps = require "rollup-plugin-sourcemaps"

_rolled = {}
roller = (opts = {}) ->
  _.defaults opts, {minify: yes, file: 'tmp/Organizer.js', skip: [], paths: {}}
  opts.skip.push mod for path, mod of opts.paths
  return rolled if rolled = _rolled[JSON.stringify opts]
  plugins = [
    coffee
      sourceMap: yes
    nodeResolve
      main: yes
      #jsnext: yes
      skip: opts.skip
      extensions: ['.js', '.coffee']
    commonjs
      extensions: ['.js', '.coffee']
      sourceMap: yes
      namedExports: opts.paths
    sourcemaps()
  ]
  plugins.push uglify() if opts.minify
  _rolled[JSON.stringify opts] = rollup.rollup
    entry: opts.file
    sourceMap: yes
    plugins: plugins

option '', '--no-browser', 'Disable targeting the browser (and AMD)'
option '', '--no-node', "Disable targeting Node.js (and CommonJS)"
option '', '--no-bare', "Disable targeting bare JS (iife)"
option '', '--no-universal', "Disable a universal target (UMD)"
option '', '--no-min', "Disable minification"
option '', '--no-map', "Disable external source maps"
option '', '--no-internal-map', "Disable internal source maps"
option '', '--no-partial', "Disable 'partial' builds, which don't include major libraries"

partialSkip = ["jquery", "lodash"]

banner = """
  /*
  CleanTile v#{require("./package.json").version}
  Copyright Ryan Leonard.  See #{require("./package.json").homepage} for LICENSE.
  */
  """

bundleGlobals =
  lodash: "_"
  jquery: "$"

browserPaths =
  "node_modules/lodash/dist/lodash.fp.js": "lodash"

buildFormat = (opts, format, ext, paths, moduleName=null) ->
  tasks = []
  tasks.push roller({paths, minify: no}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.js", sourceMap: not opts["no-map"]}
  tasks.push roller({paths, minify: no}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.inline.js", sourceMap: 'inline'} if not opts["no-internal-map"]
  tasks.push roller({paths, minify: no, skip: partialSkip}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.nolib.js", sourceMap: not opts["no-map"]} if not opts["no-partial"]
  tasks.push roller({paths, minify: no, skip: partialSkip}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.inline.nolib.js", sourceMap: 'inline'} if not opts["no-partial"] and not opts["no-internal-map"]
  return Promise.all tasks if opts["no-min"]
  tasks.push roller({paths}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.min.js", sourceMap: not opts["no-map"]}
  tasks.push roller({paths}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.inline.min.js", sourceMap: 'inline'} if not opts["no-internal-map"]
  tasks.push roller({paths, skip: partialSkip}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.nolib.min.js", sourceMap: not opts["no-map"]} if not opts["no-partial"]
  tasks.push roller({paths, skip: partialSkip}).then (bundle) -> bundle.write {moduleName, globals: bundleGlobals, banner, format, dest: "build/cleantile#{ext}.inline.nolib.min.js", sourceMap: 'internal'} if not opts["no-partial"] and not opts["no-internal-map"]
  Promise.all tasks

task 'build', 'Compile CleanTile into executable code', (opts) ->
  build opts

task 'test', 'Run tests against CleanTile', (opts) ->
  build(opts)
    .then ->
      console.log "Testing"
      exec "mocha --colors --compilers coffee:coffee-script/register"
    .then (res) ->
      console.log res.stderr
      console.log res.stdout
    .error (err) ->
      console.log "Error while building/testing: #{err}"

task 'docs', 'Build documentation pages', (opts) ->
  Promise
    .resolve exec "codo --name 'CleanTile' --title 'CleanTile Documentation' --private --readme README.md ./src - build/README.md"
    .then (res) ->
      console.log res.stderr
      console.log res.stdout
    .then ->
      replace
        regex: "v[0-9]+\.[0-9]+\.[0-9]+(?:-(?:[0-9A-Za-z-]\.?)+)?"
        replacement: "v#{require("./package.json").version}"
        paths: ['doc/']
        recursive: yes
        include: '*.html'
    .error (err) ->
      console.log "Error while creating docs: #{err}"

task 'version', 'Update the version number in documentation', (opts) ->
  replace
    regex: "v[0-9]+\.[0-9]+\.[0-9]+(?:-(?:[0-9A-Za-z-]\.?)+)?"
    replacement: "v#{require("./package.json").version}"
    paths: ['README.md']
    recursive: yes

build = (opts) ->
  Promise
    .join exec "coffee --bare --map --compile --output tmp/ src/"
    .then ->
      tasks = []
      tasks.push buildFormat opts, 'amd', '-browser', browserPaths if not opts["no-browser"]
      tasks.push buildFormat opts, 'cjs', '-node' if not opts["no-node"]
      tasks.push buildFormat opts, 'iife', '', null, 'CleanTile' if not opts["no-bare"]
      tasks.push buildFormat opts, 'umd', '-universal', browserPaths, 'CleanTile' if not opts["no-universal"]
      Promise.all tasks
    .then ->
      console.log "Compiled successfully."
    .error (err) ->
      console.log "Error while compiling: #{err}"
    .then ->
      rimraf "#{__dirname}/tmp"
