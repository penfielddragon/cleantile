AppSource = require "./AppSource"

###
Wraps a constant list of applications as an app source.
###
class StaticAppSource extends AppSource
  ###
  @param {Array<Application>} apps the applications to display to the user.
  ###
  constructor: (@apps...) ->

  ###
  @see AppSource.snapshot
  ###
  snapshot: -> @apps

module.exports = StaticAppSource
