Configuration = require "./Configuration"
AppSource = require "./AppSource"
StaticAppSource = require "./StaticAppSource"

###
Main interface to support user's editing instances of CleanTile.
###
class Organizer
  # @see AppSource
  @AppSource: AppSource
  ###
  @param {Object} options properties to set in the configuration.
  ###
  constructor: (options) ->
    @_config = new Configuration options

  ###
  Changes the source evaluated to find applications.
  @param {AppSource} source the new source for applications
  @return {Organizer}
  ###
  source: (source) ->
    @_appsource = source
    @

  ###
  Sets the app source to a static list of applications.

  @overload apps(apps)
    Use a list of applications
    @param {Array<Application>} apps all the applications to load
    @return {Organizer}

  @overload apps(app1, app2)
    Pass each application as a parameter
    @param {Application} app1 a single application to load
    @param {Application} app2 a single application to load
    @return {Organizer}
  ###
  apps: (apps...) ->
    apps = apps[0] if apps and apps.length and apps.length is 1 and apps[0].length
    @source new StaticAppSource apps
    @

  ###
  Deals with configuration.

  @overload config()
    @return {Configuration} The entire configuration object

  @overload config(key)
    Retrieve a single property's value
    @param {String} key the name of a property to read
    @return {Anything} the value stored in the configuration

  @overload config(key, val)
    Set a single property's value
    @param {String} key the name of a property to set
    @param {Anything} val the value to store
    @return {Organizer}

  @overload config(obj)
    Update multiple properties.
    @param {Object} obj names and values of configuration properties to set
    @return {Organizer}
  ###
  config: (key, val) ->
    if not key and not val
      return @_config
    if not val and typeof key isnt "object"
      @_config.get(key)
    else if not val
      @_config.extend(key)
    else
      @_config.set key, val
    @

module.exports = Organizer
