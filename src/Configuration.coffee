_ = require "lodash"

StaticLayoutSource = require "./StaticLayoutSource"

defaults =
  layouts:
    useDefaults: yes
    static:
      "Single": "{}"
      "2 Columns": "{'horiz': true, split: '50%'}"
      "3 Columns": "{'horiz': true, split: '33%', right: {'horiz': true, split: '33%'}}"
      "2 Rows": "{'horiz': false, split: '50%'}"
      "3 Rows": "{'horiz': false, split: '33%', right: {'horiz': false, split: '33%'}}"
    sources:
      "Default Layouts": StaticLayoutSource
  apps:
    sources: {}

###
Stores and sets configuration values.
###
class Configuration
  ###
  @param {Object} options properties to set in the configuration.
  ###
  constructor: (options = {}) ->
    @config = _.defaultsDeep {}, options, defaults

  ###
  Get the value of a property by name.
  @param {String} key the name of a property
  @return {Anything} the stored value
  ###
  get: (key) -> @config[key]

  ###
  Set the value of a single property.
  @param {String} key the name of a property
  @param {Anything} the value to store
  ###
  set: (key, val) -> @config[key] = val

  ###
  Update multiple properties.
  @param {Object} obj property names and associated values to set.
  ###
  extend: (obj) -> @config = _.defaultsDeep {}, obj, @config

module.exports = Configuration
