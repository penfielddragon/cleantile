###
Defines a layout source that handles the static strings in the configuration object.
###
class StaticLayoutSource

module.exports = StaticLayoutSource
