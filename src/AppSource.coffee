###
Defines a place to look for applications.  Designed to be extended, either through CoffeeScript extension or by setting
properties.

@example CoffeeScript Extension
  class RunningApps extends AppSource
    constructor: ->
      # Open a subset of apps, etc.
      @_apps = [new SettingsPane(), new TextEditorPane(), new CommentsPane(), new TextControlsPane()]
    snapshot: -> @_apps

@example JavaScript Extension
  `
  var settings = {name: "Settings", icon: "<i class='fa fa-cog'></i>"};
  var textEditor = {name: "Markdown Editor", icon: "<i class='fa fa-file-text-o'></i>"};
  var commentsPane = {name: "Comments Settings", icon: "<i class='fa fa-comment-o'></i>"};
  var textControls = {name: "Text Formatting", icon: "<i class='fa fa-sliders'></i>"};
  var runningApps = new AppSource();
  runningApps.onSnapshot(function() {
    return [settings, textEditor, commentsPane, textControls];
  });
  `
###
class AppSource
  ###
  Called by CleanTile to determine which applications can be inserted into panes.
  @abstract Override with a function that returns the current apps
  @return {Array<Application>, Promise<Array<Application>>} return either a list of applications, or a promise to return
  applications in the future.
  ###
  snapshot: -> []

  ###
  Set AppSource.snapshot on the fly.
  @see AppSource.snapshot AppSource.snapshot for description of functionality and types involved.
  @param {Function} fn a new snapshot function
  ###
  onSnapshot: (fn) -> @snapshot = fn

module.exports = AppSource
