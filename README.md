# CleanTile

A clean editor for managing tiling pane interfaces via JavaScript.

[Online Documentation](https://bbcdn.githack.com/penfielddragon/cleantile/raw/v0.0.2/doc/index.html)

### Quick Usage

```javascript

requirejs.config({
  paths: {
    "CleanTile": "https://bbcdn.githack.com/penfielddragon/cleantile/raw/v0.0.2/build/cleantile-browser.min.js"
  }
});

requirejs(["CleanTile"], function (CleanTile) {
  var tiler = new CleanTile();
  tiler.on("done", function(layout) {
    console.log(layout);
  });
  tiler.apps([
    {name: "Hello World Document", id: "helloworld"},
    {name: "README.md", id: "readme"}
  ]).show();
});
```

Not using RequireJS?  There's over 30 different build flavors, including plain JavaScript and CommonJS support,
which allows [Browserify](http://browserify.org) and more. See [build/README.md](build/README.md) for in-depth
information.
